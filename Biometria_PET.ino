//Codigo feito por Andre Cunha para o PET Eng. Comp.
//pino 2 do arduino eh o TX do sensor
//pino 3 do arduino eh o RX do sensor

#include <Adafruit_Fingerprint.h>
#include <SoftwareSerial.h>
#include <PushButton.h>
#define pinBot 11 // DEFINIÇÃO DO PINO DO BOTÃO

SoftwareSerial mySerial(2, 3); //pinos de leitura do sensor

Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);

PushButton botao(pinBot);
uint8_t id; //inteiro de 8 bits para o cadastro do id
uint8_t numMenu;


void setup() {
  Serial.begin(9600); //inicia o monitor serial do arduino, com taxa de 9600

  pinMode(12, OUTPUT);
  
  while (!Serial);
  delay(100);

 
  finger.begin(57600); // defina a taxa de dados para a porta serial do sensor
  
  if (finger.verifyPassword()) {
    Serial.println("Sensor biometrico encontrado!");
  } else {
    Serial.println("NAO foi possivel encontrar o sensor biometrico.");
    while (1) { delay(1); }
  }

  Serial.println();
  Serial.println("Precione o botao para acessar o menu");
}

//---------------------------------------------------------------------------------------------

//funcao para ler o numero id de cadastro

uint8_t readnumber(void) {
  uint8_t num = 0;
  
  while (num == 0) {
    while (! Serial.available());
    num = Serial.parseInt();
  }
  return num;
}

//---------------------------------------------------------------------------------------------

void cadastraDigital(){
   
  Serial.println("Digite a senha para cadastrar uma nova digital");
  Serial.println("SENHA:");

  if (Serial.available() > 0){
    
    uint8_t senha = readnumber();

  if(senha == 1){
    Serial.println("Digite um numero de 1 a 127 para salvar o ID da digital");
    uint8_t numID = readnumber();
    modoGravacaoID(numID);
  }

  }

}

//---------------------------------------------------------------------------------------------

void apagaDigital(){
  //funcao para apagar uma digital cadastrada


    Serial.println("Digite um numero de 1 a 127 para deletar o ID da digital");
  
    uint8_t id = readnumber();
    if (id == 0) {// ID #0 not allowed, try again!
      return;
    }

    Serial.print("Deletando ID #");
    Serial.print(id);
    Serial.println("...");
    deleteFingerprint(id);

}

  uint8_t deleteFingerprint(uint8_t id) {
  uint8_t p = -1;
  
  p = finger.deleteModel(id);

  if (p == FINGERPRINT_OK) {
    Serial.println("DELETADO!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Erro de comunicacao");
    return p;
  } else if (p == FINGERPRINT_BADLOCATION) {
    Serial.println("Nao foi possivel deletar nessa localizacao");
    return p;
  } else if (p == FINGERPRINT_FLASHERR) {
    Serial.println("Error writing to flash");
    return p;
  } else {
    Serial.print("Erro desconhecido: 0x"); Serial.println(p, HEX);
    return p;
  }   
}
  


//---------------------------------------------------------------------------------------------

void mostraDigitais(){
  
//funcao para mostrar as digitais ja cadastradas

  uint8_t p;
  uint8_t id = 0;
  
  for(int i = 0; i<128; i++, id++){

    p = finger.loadModel(id);
    
    if (p == FINGERPRINT_OK) {
      Serial.print("Digital com ID #");
      Serial.print(id);
      Serial.println(" cadastrado");
    }
  }
}


//---------------------------------------------------------------------------------------------
//funcao para ler a senha



String leStringSerial(){
  String conteudo = "";
  char caractere;
  
  // Enquanto receber algo pela serial
  while(Serial.available() > 0) {
    // Lê byte da serial
    caractere = Serial.read();
    // Ignora caractere de quebra de linha
    if (caractere != '\n'){
      // Concatena valores
      conteudo.concat(caractere);
    }
    // Aguarda buffer serial ler próximo caractere
    delay(10);
  }
    
  Serial.print("Recebi: ");
  Serial.println(conteudo);
    
  return conteudo;
}


//---------------------------------------------------------------------------------------------
//funcao para a gravacao da digital

uint8_t modoGravacaoID(uint8_t IDgravar) {

  int p = -1;
  Serial.print("Esperando uma leitura válida para gravar #"); Serial.println(IDgravar);
  delay(2000);
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Leitura concluída");
      break;
    case FINGERPRINT_NOFINGER:
      Serial.print(".");
      delay(200);
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Erro comunicação");
      break;
    case FINGERPRINT_IMAGEFAIL:
      Serial.println("Erro de leitura");
      break;
    default:
      Serial.println("Erro desconhecido");
      break;
    }
  }

  // OK successo!

  p = finger.image2Tz(1);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Leitura convertida");
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Leitura suja");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Erro de comunicação");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Não foi possível encontrar propriedade da digital");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Não foi possível encontrar propriedade da digital");
      return p;
    default:
      Serial.println("Erro desconhecido");
      return p;
  }
  
  Serial.println("Remova o dedo");
  delay(2000);
  p = 0;
  while (p != FINGERPRINT_NOFINGER) {
    p = finger.getImage();
  }
  Serial.print("ID "); Serial.println(IDgravar);
  p = -1;
  Serial.println("Coloque o Mesmo dedo novamente");
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Leitura concluída");
      break;
    case FINGERPRINT_NOFINGER:
      Serial.print(".");
      delay(200);
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Erro de comunicação");
      break;
    case FINGERPRINT_IMAGEFAIL:
      Serial.println("Erro de Leitura");
      break;
    default:
      Serial.println("Erro desconhecido");
      break;
    }
  }

  // OK successo!

  p = finger.image2Tz(2);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Leitura convertida");
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Leitura suja");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Erro de comunicação");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Não foi possível encontrar as propriedades da digital");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Não foi possível encontrar as propriedades da digital");
      return p;
    default:
      Serial.println("Erro desconhecido");
      return p;
  }
  
  // OK convertido!
  Serial.print("Criando modelo para #");  Serial.println(IDgravar);
  
  p = finger.createModel();
  if (p == FINGERPRINT_OK) {
    Serial.println("As digitais batem!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Erro de comunicação");
    return p;
  } else if (p == FINGERPRINT_ENROLLMISMATCH) {
    Serial.println("As digitais não batem");
    return p;
  } else {
    Serial.println("Erro desconhecido");
    return p;
  }   
  
  Serial.print("ID "); Serial.println(IDgravar);
  p = finger.storeModel(IDgravar);
  if (p == FINGERPRINT_OK) {
    Serial.println("Armazenado!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Erro de comunicação");
    return p;
  } else if (p == FINGERPRINT_BADLOCATION) {
    Serial.println("Não foi possível gravar neste local da memória");
    return p;
  } else if (p == FINGERPRINT_FLASHERR) {
    Serial.println("Erro durante escrita na memória flash");
    return p;
  } else {
    Serial.println("Erro desconhecido");
    return p;
  }   
}


//---------------------------------------------------------------------------------------------

int getFingerprintIDez() {
  uint8_t p = finger.getImage();
  if (p != FINGERPRINT_OK)  return -1;

  p = finger.image2Tz();
  if (p != FINGERPRINT_OK)  return -1;

  p = finger.fingerFastSearch();
  if (p != FINGERPRINT_OK)  return -1;
  
  //Encontrou uma digital!
   else {
    
     digitalWrite(12, HIGH);
     Serial.print("ID encontrado #"); Serial.print(finger.fingerID); 
     Serial.print(" com confiança de "); Serial.println(finger.confidence);
     delay(1500);
     digitalWrite(12, LOW);
     return finger.fingerID;
  } 
}

//---------------------------------------------------------------------------------------------



void loop() {
  botao.button_loop();

  if ( botao.pressed() ){
    Serial.println();
    Serial.println("Digite 1 para cadastrar nova digital");
    Serial.println("Digite 2 para remover uma digital");
    Serial.println("Digite 3 para ver as digitais cadastradas");

    numMenu = readnumber(); // Armazena caractere lido
    Serial.println();
  
    if(numMenu == 1){
      cadastraDigital();
    }

    if(numMenu == 2){
      apagaDigital();
    }

    if(numMenu == 3){
      mostraDigitais();
    }
    
    Serial.println();
  }
  
  getFingerprintIDez();


}
